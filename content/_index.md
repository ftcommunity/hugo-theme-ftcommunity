+++
title = ""
date =  2019-01-13T18:02:30+01:00
weight = 5
+++

# Dokumentation für das [Hugo](https://gohugo.io/)-Theme für die [ftcommunity](https://ftcommunity.de/). 

Das Theme basiert auf [Learn](https://github.com/matcornic/hugo-theme-learn/), 
und aktuell gibt es nur wenige Unterschiede in der Benutzung.
Von daher bitte bis auf weitere die [Dokumentation von Learn](https://learn.netlify.com/en/) benutzen.

## Unterschiede zum "Learn"-Theme

* Das Learn-Theme nutzt für den "Rahmen" der Seite die beiden Partials
`partials/header.html` und `partials/footer.html` 
wie [hier](https://gohugo.io/templates/partials/#example-header-html) beschrieben.
Das ftcommunity-Theme nutzt stattdessen die modernere Variante 
mit [Base-Templates und Blocks](https://gohugo.io/templates/base/).

* Im Learn-Theme ist das HTML für die Kopfleiste der Seite 
und die Navigation zur nächsten/vorigen Seite
hart in `partials/header.html` kodiert.
Im ftcommunity-Theme sind diese Bereiche nach 
`partials/topbar.html` und `partials/navigation.html`
ausgelagert.
