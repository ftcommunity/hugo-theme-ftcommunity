# Hugo ftcommunity theme

This repository contains the [Hugo](https://gohugo.io/) theme used for the
new [ftcommunity](https://ftcommunity.de) web site.

The theme is based on the great [Hugo Learn Theme](https://github.com/matcornic/hugo-theme-learn),
so most of its [equally great documentation](https://learn.netlify.com/en/) applies here as well.

Differences are documented at <https://ftcommunity.gitlab.io/hugo-theme-ftcommunity/>,
but note that the additional documentation is in German, since that is the language used
for the project where this theme is used.

